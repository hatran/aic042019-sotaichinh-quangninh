
/*
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));*/

$(window).resize(function() {
	resize_menu();
	resize_row_top();
});



$(function(){
	
	//Light.scrollTop('#xemtrailer_mota');
	resize_menu();
	resize_row_top();
	show_height_row_top();
	album_image();
	tab_control2();
	tab_control();
	number_thongbao();
	share_button();
	$('.back_totop').click(function() {
        $('body,html').animate({scrollTop:0},1200);
    }); 	
	$('.open_hoithi').click(function(){
		$('.wrapper_menu_footer, .br_wrapper').fadeIn();
		$(this).addClass('show_menu_footer');
		
	})
	$('.br_wrapper').click(function(){
		$('.wrapper_menu_footer, .br_wrapper').fadeOut();
		$('.open_hoithi').removeClass('show_menu_footer');
		$('ul.mobile_wrapper').slideUp();
		
	})
	
	$('.menu_all').append('<ul class="wrapper_menu mobile_wrapper"></ul>');
	$(".des_menu>li" ).clone().appendTo(".mobile_wrapper");
	$('.mobile_wrapper li').each( function(i){
		var cout_ul=0;
		$(this).children('ul').each( function(i){
			cout_ul = cout_ul + 1;
		})
		if(cout_ul > 0){
			$(this).append('<span class="icon_open_mb"><i class="fa fa-plus"></i><i class="fa fa-minus"></i></span>');
		}		
	})
	open_menu();
	
	$('.item_new').hover(function(){
		$(this).children('.wrapper_item').children('.des_new').slideDown('fast');
	}, function(){
		$(this).children('.wrapper_item').children('.des_new').slideUp('fast');
	})


	$('#list_hover span').hover(function () {
		$('#list_hover').removeAttr('class');
		 var cl  =	$(this).attr('class');
		$('#list_hover').addClass(cl+'_h').fadeIn(10000);


		
	},function (es) {

	});

	$('#list_hover span').click(function (e) {

		var cl  =	$(this).attr('class');
		cl = cl.replace('_h','');
		var letgo = $('#list_hover .'+cl);

		var _url = letgo.attr('href');
		if(_url != '' && _url != null){
			window.location.href = _url;

		}

	});












	$('.list_price').hover(function(){
	}, function(){
		$('#list_hover').removeAttr('class');
	})

	$('.btn_dangnhap').click(function() {
       $('.wrapper_login').fadeIn();
	   $('.form_dangnhap').slideDown("slow").addClass('show_login');
    }); 
	$('.wrapper_login').click(function() {
       $('.wrapper_login').fadeOut();
	   $('.form_dangnhap').slideUp("slow").removeClass('show_login');
    }); 

	$('.open_search').click(function() {
		if($('.form_search').css('display') =='none'){
			$('.form_search').slideDown(100);
			$('.tukhoa').focus();
		}
		else{
			$('.form_search').slideUp(100);
		}
        
    }); 
	
	
});

function number_thongbao(){
	var count = 0;
	var count2 = 0;
	var count3 = 0;
	$('.phapluat a').each(function(){
		count = count+ 1;
		$('<span class="number_add phapluat'+count+'">'+count+'</span>').insertBefore($(this).children('.tieude_thongbao'));
	})
	
	$('.thongbao_l a').each(function(){
		count2 = count2+ 1;
		$('<span class="number_add phapluat'+count2+'">'+count2+'</span>').insertBefore($(this).children('.tieude_thongbao'));
	})
	
	$('.dangvadoanthe a').each(function(){
		count3 = count3+ 1;
		$('<span class="number_add phapluat'+count3+'">'+count3+'</span>').insertBefore($(this).children('.tieude_thongbao'));
	})
	
	var count3 = 0;
	$('.row_left').each(function(){
		count3 = count3+ 1;
		$(this).addClass('row_left'+count3+'');
	})
}
/*-----------------------------------------
--------------Scroll mennu-----------------
------------------------------------------*/
$(window).scroll( function(){
	 $('.fix_position').each( function(i){
		var window_top = $(window).scrollTop();
		var div_top = $(this).offset().top;
		if( window_top > div_top ){
			$('.header_container').addClass('show_fix_e');
			
		}	
		else{
			$('.header_container').removeClass('show_fix_e');
		}
	})	
	
	$('.fix_nav_scroll').each( function(i){
		var window_top = $(window).scrollTop();
		var div_top = $(this).offset().top;
		if( window_top > div_top ){
			$('.nav_page').addClass('fix_nav');		
		}
		else{
			$('.nav_page').removeClass('fix_nav');
		}
	})	
	
   
})


/*-----------------------------------------
---------------share page------------------
------------------------------------------*/
function share_button(){
	$(".facebook").append('<a href="http://www.facebook.com/sharer.php?u='+ window.location.href +'" target="_blank"><i class="fa fa-facebook"></i></a>');
	$(".googleplus").append('<a href="https://plus.google.com/share?url='+ window.location.href +'" target="_blank"><i class="fa fa-google-plus"></i></a>');
	$(".twitter").append('<a href="https://twitter.com/share?url='+ window.location.href +'&amp;text=I like page&amp;" target="_blank"><i class="fa fa-twitter"></i></a>');
}


/*-----------------------------------------
---------------Load Page------------------
------------------------------------------*/
jQuery(window).load(function() {
	resize_menu();
	/*jQuery(".load_page").delay(1000).fadeOut("slow");*/
})

/*-----------------------------------------
---------------Tab Control------------------
------------------------------------------*/
function tab_control(){
	$(".column_itpc").find("[class^='tab_tt']").hide(); 
	$(".tab_tintuc a:first").attr("class","focus_active"); 
	$(".column_itpc .tab_tt1").fadeIn(); 
	$('.tab_tintuc a').click(function(e) {
		e.preventDefault();
		if ($(this).closest("a").attr("class") == "focus_active"){ 
		  return;       
		}
		else{             
		  $(".column_itpc").find("[class^='tab_tt']").hide(); 
		  $(".tab_tintuc a").attr("class",""); 
		  $(this).attr("class","focus_active"); 
		  $('.' + $(this).attr('name')).fadeIn(); 
		}
	});
}

/*-----------------------------------------
--------------tab_control2-----------------
------------------------------------------*/
function tab_control2(){
	$(".column_itpc_ev").find("[class^='tab_even']").hide(); 
	$(".tab_click a:first").attr("class","focus_active_ev"); 
	$(".column_itpc_ev .tab_even1").fadeIn(); 
	$('.tab_click a').click(function(e) {
		e.preventDefault();
		if ($(this).closest("a").attr("class") == "focus_active_ev"){ 
		  return;       
		}
		else{             
		  $(".column_itpc_ev").find("[class^='tab_even']").hide(); 
		  $(".tab_click a").attr("class",""); 
		  $(this).attr("class","focus_active_ev"); 
		  $('.' + $(this).attr('name')).fadeIn(); 
		}
	});
}

/*-----------------------------------------
--------------album Hình ảnh-----------------
------------------------------------------*/
function album_image(){
	
	 $('.album_image').each( function(i){
		  var cout_img = 0;
		 $(this).children('a').each( function(i){
			 cout_img = cout_img + 1;
			 if( cout_img <= 6){
				 $(this).addClass('item_album'+cout_img+'');
			 }
			 else{
			 }
		 })
		 $(this).children('.number_image').children('span').children('span').append('+' + (cout_img - 6));
		 
	 })
}


/*-----------------------------------------
--------------Menu Event-----------------
------------------------------------------*/
function resize_menu(){	
	if($('.open_menu').css('display') == 'block'){
		$('.wrapper_header').addClass('fix_mobile');		
	}
	else{
		$('.wrapper_header').removeClass('fix_mobile');
		$('.mobile_wrapper').fadeOut();
	}
}
function open_menu(){	
	$('.open_menu').click(function(e){
		if($('.mobile_wrapper').css('display') == 'none'){
			$('ul.mobile_wrapper').slideDown();
			$('.br_wrapper').fadeIn();
		}
		else{
			$('ul.mobile_wrapper').slideUp();
			$('.br_wrapper').fadeOut();
		}		
	})		
	
	$('.icon_open_mb').click(function(e){
		if($(this).parent('li').children('ul').css('display') == 'none'){
			$(this).parent('li').children('ul').slideDown();
						
			$(this).parent('li').children('span').children('.fa-minus').slideDown();
			$(this).parent('li').children('span').children('.fa-plus').slideUp();
		}
		else{
			$(this).parent('li').children('ul').slideUp();
			$(this).parent('li').children('span').children('.fa-minus').slideUp();
			$(this).parent('li').children('span').children('.fa-plus').slideDown();
		}
	})
}

/*-----------------------------------------
--------------Column Top-----------------
------------------------------------------*/
function resize_row_top(){	
	if($('.fix_one_row').css('display') == 'none'){
		$('.show_height_row').addClass('add_one_column');
	}
	else{
		$('.show_height_row').removeClass('add_one_column');
	}	
}
function show_height_row_top(){	
	$('.title_list_top').click(function(e){
		if($(this).parent('div').children('.add_one_column').css('display') == 'none'){
			$(this).parent('div').children('.add_one_column').slideDown();
		}
		else{
			$(this).parent('div').children('.add_one_column').slideUp();
		}
	})
}